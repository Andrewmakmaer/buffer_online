from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import redis


class Item(BaseModel):
    id: str
    text: str = None
    key: int = None


app = FastAPI()
r = redis.Redis(decode_responses=True, host="172.31.7.255", port="6379")

origins = [
    '*'
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.post("/app")
async def comment(item: Item):
    if item.id == "add_text":
        new_key = generation_new_key()
        r.set(new_key, item.text)
        return new_key
    elif item.id == "get_text":
        result = r.get(item.key)

        if result is None:
            return 'Key not found'
        else:
            r.delete(item.key)
            return result

    else:
        raise HTTPException(status_code=400, detail="Bad action id")

@app.get("/lb-check-new")
async def root():
    return {"message": "ENABLED"}


def generation_new_key():
    all_keys = r.keys()
    for i in range(1001):
        if str(i) not in all_keys:
            return i
