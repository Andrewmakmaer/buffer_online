FROM python:3

RUN mkdir -p /usr/src/app
COPY ./main.py /usr/src/app
RUN pip install fastapi && pip install "uvicorn[standard]" && pip install pydantic && pip install redis
WORKDIR /usr/src/app

EXPOSE 8000

CMD ["uvicorn", "main:app", "--reload", "--host", "0.0.0.0"]
